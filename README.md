# Discot
[![Build Status](https://travis-ci.org/Riley1994/discot.svg?branch=master)](https://travis-ci.org/Riley1994/discot)

Discot is a Java framework for creating text-command based, Discord bots.

Discot can be divided into two distinct parts the `core` and `plugins`. The `core` of Discot handles connecting to Discord, sending and recieving messages, and many other things (mainly through the [Discord4j API](https://discord4j.com)) which allows `plugins` to focus on just doing their indivdual thing.

## Getting Started With Discot
This and much, much more is coming soon! First I need to acutally get some functionality.
