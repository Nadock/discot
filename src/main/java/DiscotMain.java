/*
 * Discot - A Java framework for creating text-command based, Discord bots.
 * Copyright (C) 2017 Riley Taylor Chase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. The license should be found in the root directory
 * of the source code for this program in a file named "LICENSE", if not
 * see <http://www.gnu.org/licenses/>.
 *
 * To contact the author email: Riley1994@users.noreply.github.com
 */

public class DiscotMain {
    public static void main(String[] args) {

    }
}
